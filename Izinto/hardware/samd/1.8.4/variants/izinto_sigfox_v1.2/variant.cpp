/*
  Copyright (c) 2014-2015 Arduino LLC.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
/*
 * +------------+------------------+--------+-----------------+--------------------------------------------------------------------------------------------------------
 * + Pin number +  ZERO Board pin  |  PIN   | Label/Name      | Comments (* is for default peripheral in use)
 * +------------+------------------+--------+-----------------+--------------------------------------------------------------------------------------------------------
 * |            | Digital Low      |        |                 |
 * +------------+------------------+--------+-----------------+--------------------------------------------------------------------------------------------------------
 * | 0          | A0               |  PA02  | LED
 * | 1          | AREF             |  PA03  | VrefA - x
 * | 2          | A3               |  PA04  | VREFB - x
 * | 3          | A4               |  PA05  | RECH_BATT_LOW
 * | 4          | ~8               |  PA06  | x
 * | 5          | ~9               |  PA07  | GPS_FORCE_ON
 * | 6          | ~4               |  PA08  | SDA - Sercom 0 pad 0
 * | 7          | ~3               |  PA09  | SCL - Sercom 0 pad 1
 * | 8          | 1 <- TX          |  PA10  | GPS_VCC_EN
 * | 9          | 0 -> RX          |  PA11  | WISOL_VCC_EN
 * | 10         | 1                |  PA12  | sercom 2 pad 0 - WISOL_UART_TX
 * | 11         | ATN              |  PA13  | sercom 2 pad 1 - WISOL_UART_RX
 * | 12         | 2                |  PA14  | WISOL_RESET
 * | 13         | ~5               |  PA15  | WISOL_WAKEUP
 * | 14         | ~11              |  PA16  | sercom 1 pad 0 - uart tx to bootloader
 * | 15         | ~13              |  PA17  | sercom 1 pad 1 - uart rx from bootloader
 * | 16         | ~10              |  PA18  | SD_VCC_EN
 * | 17         | ~12              |  PA19  | SD_SPI_SS
 * | 18         | ~6               |  PA20  | sercom 3 pad 2 - SPI_MOSI
 * | 19         | 7                |  PA21  | sercom 3 pad 3 - SPI_SCK
 * | 20         |                  |  PA22  | sercom 3 pad 0 - SPI_MISO
 * | 21         |                  |  PA23  | sercom 3 pad 1 - FLASH_SPI_SS
 * | 22         |                  |  PA27  | RAIN
 * | 23         |                  |  PB22  | WIND_DIR_EN
 * | 24         |                  |  PB23  | WIND_SPEED
 * | 25         |                  |  PA28  | FLASH_VCC_EN
 * | 26         | A5               |  PB02  | PRIM_BATT_VCC
 * | 27         |                  |  PB03  | RECH_BATT_VCC
 * | 28         | A1               |  PB08  | WIND_DIRECTION
 * | 29         | A2               |  PB09  | SOLAR_VCC
 * | 30         | 4                |  PB10  | Sercom 4 pad 2 - GPS_UART_TX
 * | 31         | 3                |  PB11  | sercom 4 pad 3 - GPS_UART_RX
 * | 32         |                  |  PA24  | USB-
 * | 33         |                  |  PA25  | USB+
 PA00 and PA01 has 32kHz crystal
*/

#include "variant.h"

/*
 * Pins descriptions
 */
const PinDescription g_APinDescription[]=
{

  { PORTA,  2, PIO_ANALOG, PIN_ATTR_ANALOG, ADC_Channel0, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },      //0 LED red
  { PORTA,  3, PIO_ANALOG, PIN_ATTR_ANALOG, ADC_Channel1, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },      //1 Vcc Battery
  { PORTA,  4, PIO_ANALOG, PIN_ATTR_ANALOG, ADC_Channel4, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },      //2 VREFB
  { PORTA,  5, PIO_DIGITAL, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_5 },     //3 x
  { PORTA,  6, PIO_DIGITAL, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },     //4 x
  { PORTA,  7, PIO_DIGITAL, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },     //5 GPS force on
  { PORTA,  8, PIO_SERCOM, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },      //6 sercom 0 pad 0 - SDA
  { PORTA,  9, PIO_SERCOM, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },      //7 sercom 0 pad 1 - SCL
  { PORTA, 10, PIO_DIGITAL, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },     //8 N_VccGPS
  { PORTA, 11, PIO_DIGITAL, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },     //9 N_VccWISOL
  { PORTA, 12, PIO_SERCOM, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },      //10 sercom 2 pad 0
  { PORTA, 13, PIO_SERCOM, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },      //11 sercom 2 pad 1
  { PORTA, 14, PIO_DIGITAL, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },    //12 wisol reset
  { PORTA, 15, PIO_DIGITAL, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },    //13 wisol wakeup
  { PORTA, 16, PIO_SERCOM, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },   //14 sercom 1 pad 0
  { PORTA, 17, PIO_SERCOM, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },   //15 sercom 1 pad 1
  { PORTA, 18, PIO_DIGITAL, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },     //16 N_VccSD
  { PORTA, 19, PIO_DIGITAL, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },     //17 SPI SS SD
  { PORTA, 20, PIO_SERCOM_ALT, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },  //18 sercom 3 pad 2 - mosi
  { PORTA, 21, PIO_SERCOM_ALT, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },  //19 sercom 3 pad 3 - sck
  { PORTA, 22, PIO_SERCOM, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },   //20 sercom 3 pad 0 - miso
  { PORTA, 23, PIO_SERCOM, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },   //21 sercom 3 pad 1 - ss flash
  { PORTA, 27, PIO_DIGITAL, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_15 },  //22 Rain
  { PORTB, 22, PIO_DIGITAL, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },  //23 Wind dir pullup
  { PORTB, 23, PIO_DIGITAL, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_7 },  //24 Wind speed
  { PORTA, 28, PIO_DIGITAL, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },  //25 x
  { PORTB,  2, PIO_DIGITAL, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },  //26 N_Vcc flash
  { PORTB,  3, PIO_DIGITAL, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },     //27 LED green
  { PORTB,  8, PIO_ANALOG, (PIN_ATTR_PWM|PIN_ATTR_TIMER), ADC_Channel2, PWM4_CH0, TC4_CH0, EXTERNAL_INT_NONE },  //28 Wind dir
  { PORTB,  9, PIO_ANALOG, (PIN_ATTR_PWM|PIN_ATTR_TIMER), ADC_Channel3, PWM4_CH1, TC4_CH1, EXTERNAL_INT_NONE },  //29 LED blue
  { PORTB, 10, PIO_SERCOM_ALT, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },  //30 sercom 4 pad 2
  { PORTB, 11, PIO_SERCOM_ALT, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },  //31 sercom 4 pad 3
  
  // USB
  { PORTA, 24, PIO_COM, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE }, //32 USB/DM
  { PORTA, 25, PIO_COM, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE }, //33 USB/DP
  

} ;

extern "C" {
    unsigned int PINCOUNT_fn() {
        return (sizeof(g_APinDescription) / sizeof(g_APinDescription[0]));
    }
}

const void* g_apTCInstances[TCC_INST_NUM+TC_INST_NUM]={ TCC0, TCC1, TCC2, TC3, TC4, TC5 } ;

// Multi-serial objects instantiation
SERCOM sercom0( SERCOM0 ) ;
SERCOM sercom1( SERCOM1 ) ;
SERCOM sercom2( SERCOM2 ) ;
SERCOM sercom3( SERCOM3 ) ;
SERCOM sercom4( SERCOM4 ) ;
// SERCOM sercom5( SERCOM5 ) ;

Uart Serial( &sercom1, PIN_SERIAL_RX, PIN_SERIAL_TX, PAD_SERIAL_RX, PAD_SERIAL_TX ) ;
Uart SerialWisol( &sercom2, PIN_SERIAL_WISOL_RX, PIN_SERIAL_WISOL_TX, PAD_SERIAL_WISOL_RX, PAD_SERIAL_WISOL_TX );
Uart SerialGPS( &sercom4, PIN_SERIAL_GPS_RX, PIN_SERIAL_GPS_TX, PAD_SERIAL_GPS_RX, PAD_SERIAL_GPS_TX );


void SERCOM1_Handler()
{
  Serial.IrqHandler();
}

void SERCOM2_Handler()
{
  SerialWisol.IrqHandler();
}

void SERCOM4_Handler()
{
  SerialGPS.IrqHandler();
}


# Copyright (c) 2014-2017 Arduino LLC.  All right reserved.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA



# Izinto Sigfox Misol D21G18A
# --------------------------------------
izinto_d21g18a.name=Izinto Sigfox Misol D21G18A (UART)

izinto_d21g18a.upload.tool=bossac
izinto_d21g18a.upload.protocol=sam-ba
izinto_d21g18a.upload.maximum_size=262144
izinto_d21g18a.upload.use_1200bps_touch=true
izinto_d21g18a.upload.wait_for_upload_port=false
izinto_d21g18a.upload.native_usb=false
izinto_d21g18a.build.mcu=cortex-m0plus
izinto_d21g18a.build.f_cpu=8000000L
izinto_d21g18a.build.usb_product="Sigfox Misol"
izinto_d21g18a.build.usb_manufacturer="Izinto"
izinto_d21g18a.build.board=IZINTO_D21G18A
izinto_d21g18a.build.core=arduino
izinto_d21g18a.build.extra_flags=-D__SAMD21G18A__ {build.usb_flags}
izinto_d21g18a.build.ldscript=linker_scripts/gcc/flash_with_bootloader.ld
izinto_d21g18a.build.openocdscript=openocd_scripts/arduino_zero.cfg
izinto_d21g18a.build.variant=izinto_d21g18a
izinto_d21g18a.build.variant_system_lib=
izinto_d21g18a.build.vid=0x2341
izinto_d21g18a.build.pid=0x804d
izinto_d21g18a.bootloader.tool=openocd
izinto_d21g18a.bootloader.file=zero/samd21_sam_ba.bin

